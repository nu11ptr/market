data Order = Order
  {
    resourceQuery :: String,
    price :: Int,
    volume :: Int,
    agent :: String
  } derive(Show, Eq)

